"""
Template for implementing QLearner  (c) 2015 Tucker Balch

Copyright 2018, Georgia Institute of Technology (Georgia Tech)
Atlanta, Georgia 30332
All Rights Reserved

Template code for CS 4646/7646

Georgia Tech asserts copyright ownership of this template and all derivative
works, including solutions to the projects assigned in this course. Students
and other users of this template code are advised not to share it with others
or to make it available on publicly viewable websites including repositories
such as github and gitlab.  This copyright statement should not be removed
or edited.

We do grant permission to share solutions privately with non-students such
as potential employers. However, sharing with other current or future
students of CS 7646 is prohibited and subject to being investigated as a
GT honor code violation.

-----do not edit anything above this line---

Student Name: Brandon Sheffield
GT User ID: bsheffield7
GT ID: 903312988
"""

import random as rand
import numpy as np


class QLearner(object):

    def __init__(self, \
                 num_states=100, \
                 num_actions=4, \
                 alpha=0.2, \
                 gamma=0.9, \
                 rar=0.5, \
                 radr=0.99, \
                 dyna=0, \
                 verbose=False):
        """
        This is a Q learner object.

        :param num_states: The number of states to consider.
        :type num_states: int
        :param num_actions: The number of actions available..
        :type num_actions: int
        :param alpha: The learning rate used in the update rule. Should range between 0.0 and 1.0 with 0.2 as a typical value.
        :type alpha: float
        :param gamma: The discount rate used in the update rule. Should range between 0.0 and 1.0 with 0.9 as a typical value.
        :type gamma: float
        :param rar: Random action rate: the probability of selecting a random action at each step. Should range between 0.0 (no random actions) to 1.0 (always random action) with 0.5 as a typical value.
        :type rar: float
        :param radr: Random action decay rate, after each update, rar = rar * radr. Ranges between 0.0 (immediate decay to 0) and 1.0 (no decay). Typically 0.99.
        :type radr: float
        :param dyna: The number of dyna updates for each regular update. When Dyna is used, 200 is a typical value.
        :type dyna: int
        :param verbose: If “verbose” is True, your code can print out information for debugging.
        :type verbose: bool
        """

        self.num_states = num_states
        self.num_actions = num_actions
        self.alpha = alpha
        self.gamma = gamma
        self.rar = rar
        self.radr = radr
        self.dyna = dyna
        self.verbose = verbose

        self.s = 0
        self.a = 0

        self.Q = np.zeros((num_states, num_actions))  # initialize Q[] with all zeros
        self.Model = np.zeros((0, 4), dtype=int)

    def querysetstate(self, s):
        """
        Update the state without updating the Q-table

        :param s: The new state
        :type s: int
        :return: The selected action
        :rtype: int
        """

        self.s = s

        if np.random.uniform() < self.rar:
            action = rand.randint(0, self.num_actions - 1)
        else:
            action = np.argmax(self.Q[s, :])

        if self.verbose: print(f"s = {s}, a = {action}")

        return action

    def query(self, s_prime, r):
        """
         Update the Q table and return an action

         :param s_prime: The new state
         :type s_prime: int
         :param r: The immediate reward
         :type r: float
         :return: The selected action
         :rtype: int
         """

        self.q_tabular_update(self.s, self.a, s_prime, r)

        if self.dyna > 0:
            self.dyna_q(r, s_prime)

        if self.rar > rand.uniform(0, 1):
            self.a = rand.randint(0, self.num_actions - 1)
        else:
            self.a = np.argmax(self.Q[s_prime, :])

        self.s = s_prime
        self.rar *= self.radr

        if self.verbose:
            print(f"s = {s_prime}, a = {self.a}, r={r}")

        return self.a

    def dyna_q(self, r, s_prime):
        """
        Exploits a middle ground, yielding strategies that are both more effective than model-free learning and
         more computationally effecient than the certainty-equivalence approach. The implemented approach uses the
          Dyna-Q algorithm from Slide 8 from the following source:
          https://people.cs.umass.edu/~barto/courses/cs687/Chapter%209.pdf
        :param r: The reward
        :param s_prime: The new state
        :return:
        """

        # Choose a random previously observed state
        self.Model = np.r_[self.Model, np.array([[self.s, self.a, s_prime, r]])]
        rand_choice = np.random.choice(self.Model.shape[0], self.dyna, replace=True)

        # Choose a random previously observed state
        random_state = self.Model[rand_choice, 0]

        # multiple "hallucinated" experience tuples are used to update the Q-table for each "real" experience
        for i in range(len(random_state)):
            # take random action previously taken in randomly previously observed state
            self.q_tabular_update(self.Model[rand_choice, 0][i], self.Model[rand_choice, 1][i],
                                  self.Model[rand_choice, 2][i],
                                  self.Model[rand_choice, 3][i])

    def q_tabular_update(self, s, a, s_prime, r):
        """
        Helper function used to update the Q table using one-step tabular Q-learning
        Source: https://people.cs.umass.edu/~barto/courses/cs687/Chapter%209.pdf
        Slide 8 Dyna-Q Algorithm: Q(s,a) <- Q(s,a)+ alpha[R(s,a,s') + y * max(s',a') - Q(s,a)]
        :param s: state
        :param a: action
        :param s_prime: New state
        :param r: reward
        :return:
        """

        self.Q[s, a] = self.Q[s, a] + self.alpha * (
                r + self.gamma * max(self.Q[s_prime, :]) - self.Q[s, a])

    def author(self):

        return "bsheffield7"


if __name__ == "__main__":
    print("Remember Q from Star Trek? Well, this isn't him")
